import shutil
import os
import json


def print_info(title):
    print(title)


def create_dist(dist):
    try:
        os.mkdir(dist)
        print(f'Folder {dist} has been created')
    except FileExistsError:
        print(f'Folder {dist} already exists')


def copy_dot(path, dist):
    shutil.copy(path, dist)
    print(f'Config. file {path} has been updated\n')


apps = json.load(open('dotfiles'))

print('DOTCollector v.0.0.2')
print('There are available options:\n'
      '\t1) config. files update\n'
      '\t2) list of config. files\n'
      '\t3) add a new config. file to sync. list\n'
      '\t4) delete config. file from sync. list.\n'
      '\t5) quit')

com = int(input('Enter what do you want, 1-5: '))

if com == 1:
    for app in apps:
        print_info(app.get('title'))
        create_dist(app.get('dist'))
        copy_dot(app.get('path'), app.get('dist'))

elif com == 2:
    for app in apps:
        print_info(app.get('title'))

elif com == 3:
    title = (input('Enter a name of program: '))
    path = (input('Enter a path of config. file: '))
    dist = (input('Enter a path of folder in config. files repository: '))
    newapp = {'title': title, 'path': path, 'dist': dist}
    apps.append(newapp)
    json.dump(apps, open('dotfiles', 'w'))

elif com == 4:
    appdelete = str(input('Enter a name of program for delete config.file: '))
    flag = True
    for app in range(len(apps)):
        if apps[app]['title'] == appdelete:
            del apps[app]
            json.dump(apps, open('dotfiles', 'w'))
            print(f'Config. file of "{appdelete}" deleted from sync. list')
            flag = False
            break
    if flag:
        print(f'Programm "{appdelete}" not found in the sync. list')

elif com == 5:
    exit()

else:
    print('You have entered a wrong command')
